from finaledit import executing,daily_query
import csv
import sys, getopt
import socket,virustotal2
from dns_resolver import get_ip,dns_records,get_host,get_ASN
from whois_record import whois_records
from dns import resolver
from dns import reversename
import tldextract
import urllib2, urllib, sys, json

vt = virustotal2.VirusTotal2("51b537223a305cfdac78a16501f593af83f1fdbbc8498c9c978285753bd863a8")
def get_TLD_1(domain):
    ext = tldextract.extract(domain)
    host = ext.domain
    if ext.suffix != '':
        host += "." + ext.suffix
    return host

def main(argv):
    global host_ip_dict
    whoisrecords = ''
    active_dns = ''
    co_locateddomains = ''
    per_malicious = ''
    list_colocated_domains=''
    blacklisted_domain=''
    detected_domain=''
    try:
        opts, args = getopt.getopt(argv, 'w:a:c:m:l:b:d::')
    except getopt.GetoptError:
        print 'usage: python DNSinject.py -i <interface> <optional expression>'
        print '   or: python DNSinject.py -f <filename> <optional expression>'
        sys.exit()
    
    for opt, arg in opts:
        if opt == '-w':
        	whoisrecords = arg
        elif opt == '-a':
        	active_dns = arg
	elif opt =='-c':
		co_locateddomains=arg
	elif opt=='-m':
		per_malicious=arg
	elif opt=='-b':
		blacklisted_domain=arg
	elif opt=='-d':
		detected_domain=arg
    
    print 'Following are the details:\n'
    
    if whoisrecords != '' :
        	print '******** whois records for:' + whoisrecords+'************\n'
		wh, whdata=whois_records(whoisrecords)
		print wh
		print whdata
        	#sys.exit()
    if active_dns!='' :
		print '********DNS data for:' + active_dns +'  ************\n'	
		ip=get_ip(active_dns)
		print(ip)
		#name = socket.gethostbyaddr(ip)
		print 'IP address of domain:' +ip
		print 'Host:' + active_dns
		print 'ASN:' + str(get_ASN(ip))
        	print "DNS records" 
		print dns_records(active_dns)
    if co_locateddomains!='':
	try:
		print '********Co_located domains for:' + co_locateddomains +'  ************\n'
		count=0
		ip=get_ip(co_locateddomains)
		ip_report = vt.retrieve(ip)
		uri = "http://reverseip.logontube.com/?url=%s&output=json"%(co_locateddomains)
		result=urllib2.urlopen(uri).read()
		obj = json.loads(result)
		print "No of domains:" + obj['response']['domain_count']
		print "----Domain names--------"
		for domain in obj['response']['domains']:
			domain=domain.split(",");
			print domain
		for u in ip_report.detected_urls:
			if u["positives"]>0:
				print get_TLD_1(u["url"])
				count+=1
	except:
		pass
    if per_malicious!='':
	try:
		print '********Malicious domains details' +'  ************\n'
		ip=get_ip(per_malicious)
		ip_report = vt.retrieve(ip)
		total_pos = sum([u["positives"] for u in ip_report.detected_urls])
    		total_scan = sum([u["total"] for u in ip_report.detected_urls])
    		count = len(ip_report.detected_urls)
		if(count==0):
			print "No malicious domains";
		count1=0
   		print str(count)+" URLs hosted on "+per_malicious+":"+ip+" are called malicious by (on average) " + \
          str(int(total_pos/count)) + " / " + str(int(total_scan/count)) + " scanners"
		for u in ip_report.detected_urls:
			#if u["positives"]>0:
				print u["url"]
				count+=1
		print count
	except ZeroDivisionError as detail:
		pass

    if blacklisted_domain!='':	
	tld=get_TLD_1(blacklisted_domain)
	file=open('malware_domains.txt')
	file1=open('all_domains.csv')
	malware=[]
	lines = (line.rstrip('\n') for line in file)
	lines_domain = (line.rstrip('\n') for line in file1)
	for item in lines:
		malware.append(get_TLD_1(item))
        for item in malware:
		if item == tld:
			print tld + " is Blacklisted domain"
		for domain in lines_domain:
			domain=get_TLD_1(domain)
			if item == domain:
				print domain + " is Blacklisted domain"

    if detected_domain!='':
		print "****** Detected Domains ********"
		ip=get_ip(per_malicious)
		ip_report = vt.retrieve(ip)
		count=0
		for u in ip_report.detected_urls:
			if u["positives"] >0:
				count+=1
		print count
    if whoisrecords=='' and active_dns=='' and co_locateddomains=='' and per_malicious=='' and detected_domain=='' and blacklisted_domain=='' :
	executing('net1.sql')
	with open('top1m.csv', 'rb') as f:
    		reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
    		for row in reader:
			d = row[1].strip()
			print d
			p=daily_query.delay(d)
			print p.get()
			
if __name__ == "__main__":
    main(sys.argv[1:])


